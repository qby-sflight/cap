using {
    Currency,
    managed
} from '@sap/cds/common';

namespace sflight.db;

entity Travels : managed {
    key ID           : UUID;
        beginDate    : Date;
        endDate      : Date;
        bookingFee   : Decimal(16, 3);
        totalPrice   : Decimal(16, 3);
        currencyCode : Currency;
        description  : String(1024);
};
